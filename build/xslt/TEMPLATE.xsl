<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!--
  To externalise xsl code to a new file, copy this template,
  copy the new content to it, and replace the according section
  in the originating file with an include like this:

  <xsl:include href="template.xsl" />
  -->

</xsl:stylesheet>
